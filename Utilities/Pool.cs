using System;
using System.Collections.Generic;

namespace Murk.Utilities
{
    public interface IPoolable 
    {
        void Reset();
    }

    public class Pool<T> where T : class, IPoolable
    {
        private readonly Stack<T> _items;
        private readonly Func<T> _constructor;
        private readonly int _initialCapacity;

        public Pool(Func<T> constructor, int capacity)
        {
            _constructor = constructor ?? throw new ArgumentNullException(nameof(constructor));
            _initialCapacity = capacity;

            _items = new Stack<T>(capacity);
        }

        public void Reset()
        {
            _items.Clear();
        }

        public T Get()
        {
            if (!_items.TryPop(out var item))
            {
                Populate();
                item = _items.Pop();
            }

            return item;
        }

        public void Recycle(T item)
        {
            item.Reset();
            _items.Push(item);
        }

        private void Populate()
        {
            for (var i = 0; i < _initialCapacity; i++)
            {
                var item = _constructor();
                item.Reset();
                _items.Push(item);
            }
        }
    }
}