namespace Murk.Utilities
{
    public static class Math
    {
        public static int Lerp(int start, int goal, float amount)
        {
            return (int)(start + (goal - start) * amount);
        }

        public static int Sqrt(int number)
        {
            // taken from https://stackoverflow.com/a/16366529
            var s = 1;
            var t = number;
            var x = t;

            while (s < t)
            {
                s <<= 1;
                t >>= 1;
            } //decide the value of the first tentative

            do
            {
                t = s;
                s = (x / s + s) >> 1; //x1=(N / x0 + x0)/2 : recurrence formula
            } while (s < t);

            return t;
        }
    }
}