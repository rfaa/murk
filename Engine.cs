using System;
using Murk.Commands;
using Murk.Components;
using Murk.Entities;
using Murk.Networking;
using Murk.Networking.Packets;
using Murk.States;
using Murk.Systems;
using Murk.Systems.Draw;
using Murk.Systems.Update;
using Raylib;
using static Raylib.Raylib;

namespace Murk
{
    public class Engine : IDisposable
    {
        private readonly StateManager _stateManager;
        private readonly NetworkClientManager _networkClientManager;
        private readonly NetworkServerManager _networkServerManager;
        private readonly ServerManager _serverManager;
        private readonly SystemManager _systemManager;
        private readonly ComponentManager _componentManager;
        private readonly EntityManager _entityManager;

        private readonly ComponentFactory _componentFactory;
        private readonly EntityFactory _entityFactory;
        private readonly CommandFactory _commandFactory;
        private readonly PacketFactory _packetFactory;
        
        private float _deltaAccumulator;
        private int _currentFramesPerSecond;

        public Engine()
        {
            _stateManager = new StateManager();
            _systemManager = new SystemManager();
            _componentManager = new ComponentManager();

            _componentFactory = new ComponentFactory();
            _entityFactory = new EntityFactory(_componentManager, _componentFactory);
            _commandFactory = new CommandFactory(_entityFactory);
            _packetFactory = new PacketFactory(_commandFactory);

            _entityManager = new EntityManager(_entityFactory);
            _networkClientManager = new NetworkClientManager(_packetFactory);
            _networkServerManager = new NetworkServerManager(_packetFactory);
            _serverManager = new ServerManager(_networkServerManager, _commandFactory);
        }

        public void Initialize()
        {
            // TODO: fix dependency injection
            // TODO: also look into corert, might actually need this poor man's dependency injection then

            TraceLog((int)Raylib.TraceLogType.LOG_INFO, "Initializing murk engine");

            //SetConfigFlags(FLAG_VSYNC_HINT);
            InitWindow(1280, 720, "murk");

            _commandFactory.Initialize();
            _componentFactory.Initialize();
            _packetFactory.Initialize();
            _entityFactory.Initialize();

            _networkClientManager.Initialize();
            _networkServerManager.Initialize();
            _serverManager.Initialize();
            _entityManager.Initialize();
            _stateManager.Initialize(
                new MainState(_stateManager, _networkClientManager, _networkServerManager),
                new LobbyState(_stateManager, _networkClientManager, _networkServerManager),
                new PlayState(_stateManager, _systemManager, _networkClientManager, _entityManager, _entityFactory, _commandFactory),
                new MenuState(_stateManager));
            _systemManager.Initialize(
                new IUpdateSystem[]
                {
                    // systems are run in the order they are added
                    new PreviousBodyUpdateSystem(_componentManager),
                    new MovementUpdateSystem(_componentManager, _componentFactory),
                    new KillUpdateSystem(_entityFactory, _componentFactory)
                },
                new IDrawSystem[]
                {
                    // systems are run in the order they are added
                    new BodyDrawSystem(_componentManager)
                });

            _stateManager.SetState<MainState>();
        }

        public void Run()
        {
            var ticks = 0L;
            var delta = 0F;
            var elapsed = TimeSpan.Zero;

            var tickAccumulator = 0L;
            var tickSize = TimeSpan.TicksPerSecond / 20;

            while (!WindowShouldClose())
            {
                // get how long time has passed since last draw
                elapsed = TimeSpan.FromSeconds(GetFrameTime());

                // fixed interval update loop
                tickAccumulator += elapsed.Ticks;
                while (tickAccumulator > tickSize)
                {
                    Update();
                    tickAccumulator -= tickSize;
                    ticks++;
                }

                // handle input as often as possible
                Input();

                // calculate the delta so we can interpolate
                delta = tickAccumulator / (float)tickSize;

                // draw as often as possible
                Draw(delta);
            }
        }

        public void Dispose()
        {
            CloseWindow();
        }

        private void Update()
        {
            _networkServerManager.Update();
            _networkClientManager.Update();
            _stateManager.Update();
        }

        private void Input()
        {
            _stateManager.Input();
        }

        private void Draw(float delta)
        {
            // this is needed because we don't want to update the fps too often, it gets impossble to read
            if ((_deltaAccumulator += delta) > 10F)
            {
                _currentFramesPerSecond = GetFPS();
                _deltaAccumulator = 0F;
            }

            ClearBackground(Color.BLACK);
            BeginDrawing();

            _stateManager.Draw(delta);
            
            DrawText($"FPS {_currentFramesPerSecond}", 0, 0, 10, Color.WHITE);
            
            EndDrawing();
        }
    }
}