namespace Murk.Components.Utilities
{
    public class ToggleComponent : IComponent
    {
        public bool Toggle { get; set; }

        public void Reset()
        {
            Toggle = false;
        }
    }
}