namespace Murk.Components.Utilities
{
    public class CounterComponent : IComponent
    {
        public int Counter { get; set; }

        public void Reset()
        {
            Counter = 0;
        }
    }
}