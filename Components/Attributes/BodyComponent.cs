using Raylib;

namespace Murk.Components.Attributes
{
    public class BodyComponent : IComponent
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Radius { get; set; }
        public int Speed { get; set; }
        public int PreviousX { get; set; }
        public int PreviousY { get; set; }
        public int PreviousRadius { get; set; }

        public void Reset()
        {
            X = Y = Radius = Speed = 0;
            PreviousX = PreviousY = PreviousRadius = 0;
        }
    }
}