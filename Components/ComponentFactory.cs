using System;
using System.Collections.Generic;
using Murk.Components.Attributes;
using Murk.Components.Utilities;
using Murk.Utilities;

namespace Murk.Components
{
    public class ComponentFactory
    {
        private readonly Dictionary<Type, Pool<IComponent>> _pools;

        public ComponentFactory()
        {
            _pools = new Dictionary<Type, Pool<IComponent>>();
        }

        public void Initialize()
        {
            InitializePool(() => new BodyComponent());
            InitializePool(() => new ToggleComponent());
            InitializePool(() => new CounterComponent());
        }

        public T Get<T>() where T : class, IComponent
        {
            return (T)_pools[typeof(T)].Get();
        }

        public void Recycle<T>(T component) where T : class, IComponent
        {
            _pools[typeof(T)].Recycle(component);
        }

        private void InitializePool<T>(Func<T> ctor) where T : class, IComponent
        {
            _pools[typeof(T)] = new Pool<IComponent>(ctor, 256);
        }
    }
}