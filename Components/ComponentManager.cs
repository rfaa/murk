using System;
using System.Collections.Generic;
using Murk.Entities;

namespace Murk.Components
{
    public class ComponentManager
    {
        private Dictionary<Type, Dictionary<int, Action<IComponent, Entity>>> _onComponentAddedCallbacks;
        private Dictionary<Type, Dictionary<int, Action<IComponent, Entity>>> _onComponentRemovedCallbacks;

        public ComponentManager()
        {
            _onComponentAddedCallbacks = new Dictionary<Type, Dictionary<int, Action<IComponent, Entity>>>();
            _onComponentRemovedCallbacks = new Dictionary<Type, Dictionary<int, Action<IComponent, Entity>>>();
        }

        public void Subscribe<T>(Action<T, Entity> onComponentAdded, Action<T, Entity> onComponentRemoved) 
            where T : class, IComponent
        {
            if (!_onComponentAddedCallbacks.TryGetValue(typeof(T), out var callbacksAdded))
            {
                callbacksAdded = new Dictionary<int, Action<IComponent, Entity>>();
                _onComponentAddedCallbacks.Add(typeof(T), callbacksAdded);
            }

            callbacksAdded.Add(onComponentAdded.GetHashCode(), (component, entity) => 
            {
                onComponentAdded((T)component, entity);
            });

            if (!_onComponentRemovedCallbacks.TryGetValue(typeof(T), out var callbacksRemoved))
            {
                callbacksRemoved = new Dictionary<int, Action<IComponent, Entity>>();
                _onComponentRemovedCallbacks.Add(typeof(T), callbacksRemoved);
            }

            callbacksRemoved.Add(onComponentRemoved.GetHashCode(), (component, entity) => 
            {
                onComponentRemoved((T)component, entity);
            });
        }

        public void Unsubscribe<T>(Action<T, Entity> onComponentAdded, Action<T, Entity> onComponentRemoved)
        {
            if (_onComponentAddedCallbacks.TryGetValue(typeof(T), out var callbacksAdded))
            {
                callbacksAdded.Remove(onComponentAdded.GetHashCode());
            }

            if (_onComponentRemovedCallbacks.TryGetValue(typeof(T), out var callbacksRemoved))
            {
                callbacksRemoved.Remove(onComponentRemoved.GetHashCode());
            }
        }

        public void ComponentAdded<T>(T component, Entity entity) where T : class, IComponent
        {
            if (_onComponentAddedCallbacks.TryGetValue(typeof(T), out var callbacks))
            {
                foreach (var callback in callbacks.Values)
                {
                    callback((IComponent)component, entity);
                }
            }
        }

        public void ComponentRemoved<T>(T component, Entity entity) where T : class, IComponent
        {
            if (_onComponentRemovedCallbacks.TryGetValue(typeof(T), out var callbacks))
            {
                foreach (var callback in callbacks.Values)
                {
                    callback((IComponent)component, entity);
                }
            }
        }
    }
}