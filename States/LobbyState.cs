using System;
using Murk.Networking;
using Murk.Networking.Packets.Lobby;
using static Raylib.Raylib;

namespace Murk.States
{
    public class LobbyState : IState
    {
        private readonly StateManager _stateManager;
        private readonly NetworkClientManager _networkClientManager;
        private readonly NetworkServerManager _networkServerManager;
        private readonly LobbyActions _actions;

        public LobbyState(StateManager stateManager, NetworkClientManager networkClientManager, 
            NetworkServerManager networkServerManager)
        {
            _stateManager = stateManager ?? throw new ArgumentNullException(nameof(stateManager));
            _networkClientManager = networkClientManager ?? throw new ArgumentNullException(nameof(networkClientManager));
            _networkServerManager = networkServerManager ?? throw new ArgumentNullException(nameof(networkServerManager));

            _actions = new LobbyActions();
        }

        public void Initialize()
        {
        }

        public void Enter()
        {
            _networkClientManager.Subscribe<LobbyStartPacket>(ReceivedLobbyStart);
            _networkClientManager.Subscribe<LobbyStopPacket>(ReceivedLobbyStop);
        }

        public void Exit()
        {
            _networkClientManager.Unsubscribe<LobbyStartPacket>(ReceivedLobbyStart);
            _networkClientManager.Unsubscribe<LobbyStopPacket>(ReceivedLobbyStop);

            _actions.Reset();
        }

        public void Update()
        {
            if ((_actions.Progress += GetRandomValue(4, 10)) > 100) { _actions.Progress = 100; }

            if (_actions.IsRequestingStart)
            {
                _networkClientManager.Send<LobbyStartPacket>();
                _actions.IsRequestingStart = false;
            }

            if (_actions.IsRequestingStop)
            {
                _networkClientManager.Send<LobbyStopPacket>();
                _actions.IsRequestingStop = false;
            }

            if (_actions.IsStarting)
            {
                if (_actions.StartTime - DateTime.UtcNow <= TimeSpan.Zero)
                {
                    _stateManager.SetState<PlayState>();
                }
            }

            if (_actions.IsExiting)
            {
                _stateManager.SetState<MainState>();
                _networkClientManager.Disconnect();
                _networkServerManager.Stop();
            }
        }

        public void Input()
        {
            if (IsKeyPressed(Raylib.Raylib.KEY_S) && _actions.Progress == 100)
            {
                if (_actions.IsStarting)
                {
                    _actions.IsRequestingStop = true;
                }
                else
                {
                    _actions.IsRequestingStart = true;
                }
            }

            if (IsKeyPressed(Raylib.Raylib.KEY_E))
            {
                _actions.IsExiting = true;
            }
        }

        public void Draw(float delta)
        {
            DrawText("lobby", 1280 / 2 - MeasureText("lobby", 20) / 2, 720 / 8, 20, Raylib.Color.WHITE);
            DrawText($"{_actions.Progress}", 1280 / 2 - MeasureText($"100", 10) / 2, 720 / 5, 10, Raylib.Color.WHITE);

            if (_actions.IsStarting)
            {
                var remaining = (_actions.StartTime - DateTime.UtcNow).Seconds + 1;
                if (remaining < 0) { remaining = 0; }
                DrawText($"starting {remaining}", 1280 / 2 - MeasureText($"starting 3", 10) / 2, 720 / 6, 10, Raylib.Color.WHITE);
                
                DrawText("[s]top ", 1280 / 2 - MeasureText("[s]top ", 10) / 2, 720 / 4, 10, Raylib.Color.WHITE);
            }
            else
            {
                DrawText("[s]tart", 1280 / 2 - MeasureText("[s]tart", 10) / 2, 720 / 4, 10, Raylib.Color.WHITE);
            }

            
            DrawText("[e]xit", 1280 / 2 - MeasureText("[e]xit", 10) / 2, 720 / 4 + 12, 10, Raylib.Color.WHITE);
        }

        private void ReceivedLobbyStart(LobbyStartPacket packet)
        {
            _actions.IsStarting = true;
            _actions.StartTime = DateTime.UtcNow.AddSeconds(3);
        }

        private void ReceivedLobbyStop(LobbyStopPacket packet)
        {
            _actions.IsStarting = false;
        }

        private class LobbyActions
        {
            public int Progress { get; set; }
            public bool IsExiting { get; set; }
            public bool IsStarting { get; set; }
            public bool IsRequestingStart { get; set; }
            public bool IsRequestingStop { get; set; }
            public DateTime StartTime { get; set; }

            public void Reset()
            {
                Progress = 0;
                IsExiting = false;
                IsStarting = false;
                IsRequestingStart = false;
                IsRequestingStop = false;
                StartTime = DateTime.MinValue;
            }
        }
    }
}