using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using Murk.Commands;
using Murk.Components;
using Murk.Components.Attributes;
using Murk.Entities;
using Murk.Networking;
using Murk.Networking.Packets.Play;
using Murk.Systems;
using static Raylib.Raylib;

namespace Murk.States
{
    public class PlayState : IState
    {
        private readonly StateManager _stateManager;
        private readonly SystemManager _systemManager;
        private readonly NetworkClientManager _networkClientManager;
        private readonly EntityManager _entityManager;
        private readonly EntityFactory _entityFactory;
        private readonly CommandFactory _commandFactory;
        private readonly PlayActions _actions;
        private readonly LinkedList<ICommand> _queuedCommands;
        private readonly Dictionary<int, ICommand[][]> _playersCommands;
        private readonly Dictionary<int, int> _playerIndexes;

        private int _tick;
        private bool _isStalling;

        public PlayState(StateManager stateManager, SystemManager systemManager, NetworkClientManager networkClientManager,
            EntityManager entityManager, EntityFactory entityFactory, CommandFactory commandFactory)
        {
            _stateManager = stateManager ?? throw new ArgumentNullException(nameof(stateManager));
            _systemManager = systemManager ?? throw new ArgumentNullException(nameof(systemManager));
            _networkClientManager = networkClientManager ?? throw new ArgumentNullException(nameof(networkClientManager));
            _entityManager = entityManager ?? throw new ArgumentNullException(nameof(entityManager));
            _entityFactory = entityFactory ?? throw new ArgumentNullException(nameof(entityFactory));
            _commandFactory = commandFactory ?? throw new ArgumentNullException(nameof(commandFactory));

            _actions = new PlayActions();
            _queuedCommands = new LinkedList<ICommand>();
            _playerIndexes = new Dictionary<int, int>();
            _playersCommands = new Dictionary<int, ICommand[][]>();
        }

        public void Initialize()
        {
        }

        public void Enter()
        {
            _networkClientManager.Subscribe<PlayUpdatePacket>(OnPlayUpdatePacketReceived);
            _networkClientManager.Subscribe<PlayPausePacket>(OnPlayPausePacketReceived);
            _networkClientManager.Subscribe<PlayResumePacket>(OnPlayResumePacketReceived);

            // TOOD: initialize player indexes with all clients
            _playerIndexes[0] = 0;

            // send a play start packet so the server can send us the first empty play update packets
            _networkClientManager.Send<PlayStartPacket>();
        }

        public void Exit()
        {
            _networkClientManager.Unsubscribe<PlayUpdatePacket>(OnPlayUpdatePacketReceived);
            _networkClientManager.Unsubscribe<PlayPausePacket>(OnPlayPausePacketReceived);
            _networkClientManager.Unsubscribe<PlayResumePacket>(OnPlayResumePacketReceived);
        }

        public void Update()
        {
            if (_actions.IsEnteringMenuState) { _stateManager.SetState<MenuState>(false); }

            _isStalling = 
                   !_playersCommands.TryGetValue(_tick, out var playersCommands)
                || playersCommands.Any(x => x == null);

            if (_isStalling) { return; }

            for (var cmdNode = _queuedCommands.First; cmdNode != null; cmdNode = cmdNode.Next)
            {
                // TODO: check if command is currently valid, if not then remove it to save resources

                // just send one command for now...
                if (cmdNode.Next != null) { _queuedCommands.Remove(cmdNode); }
            }

            // send the updates for the current tick
            _networkClientManager.Send<PlayUpdatePacket>(x => 
            {
                x.Tick = _tick;
                //x.Commands = _queuedCommands.ToArray();
                x.Commands = new ICommand[_queuedCommands.Count > 0 ? 1 : 0];
                if (x.Commands.Length > 0)
                {
                    x.Commands[0] = _queuedCommands.First.Value;
                }
            });

            // clear the current command queue
            _queuedCommands.Clear();

            // run all players' commands
            foreach (var playerCommands in playersCommands)
            {
                foreach (var playerCommand in playerCommands)
                {
                    playerCommand.Execute();
                }
            }

            // update them systems
            _systemManager.Update();

            _tick++;
        }

        public void Input()
        {
            if (IsKeyPressed(Raylib.Raylib.KEY_P))
            {
                _actions.IsEnteringMenuState = true;
            }

            if (IsKeyPressed(Raylib.Raylib.KEY_C) || IsKeyDown(Raylib.Raylib.KEY_C))
            {
                _queuedCommands.AddLast(_commandFactory.Get<SpawnCommand>(x => 
                {
                    x.X = GetMouseX() * 100;
                    x.Y = GetMouseY() * 100;
                    x.EntityType = EntityType.Ship;
                }));
            }
        }

        public void Draw(float delta)
        {
            DrawText("play", 1280 / 2 - MeasureText("play", 20) / 2, 720 / 8, 20, Raylib.Color.WHITE);
            
            DrawText($"{_entityManager.EntityCount} ENT", 1280 - MeasureText($"{_entityManager.EntityCount} ENT", 10), 0, 10, Raylib.Color.WHITE);
            
            if (_isStalling)
            {
                DrawText($"stalling {_tick}", 1280 / 2 - MeasureText($"stalling {_tick}", 10) / 2, 720 / 6, 10, Raylib.Color.WHITE);
            }

            DrawText("[c]reate", 1280 / 2 - MeasureText("[c]reate", 10) / 2, 720 / 4, 10, Raylib.Color.WHITE);

            _systemManager.Draw(delta);
        }

        private void OnPlayUpdatePacketReceived(PlayUpdatePacket packet)
        {
            if (!_playersCommands.TryGetValue(packet.Tick, out var commands))
            {
                commands = new ICommand[_playerIndexes.Count][];
                _playersCommands.Add(packet.Tick, commands);
            }

            commands[_playerIndexes[packet.ClientId]] = packet.Commands;
        }

        private void OnPlayPausePacketReceived(PlayPausePacket packet)
        {

        }

        private void OnPlayResumePacketReceived(PlayResumePacket packet)
        {
        }

        private class PlayActions
        {
            public bool IsEnteringMenuState { get; set; }

            public void Reset()
            {
                IsEnteringMenuState = false;
            }
        }
    }
}