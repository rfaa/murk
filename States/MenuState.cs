using System;
using static Raylib.Raylib;

namespace Murk.States
{
    public class MenuState : IState
    {
        private readonly StateManager _stateManager;
        private readonly MenuActions _actions;

        private PlayState _playState;

        public MenuState(StateManager stateManager)
        {
            _stateManager = stateManager ?? throw new ArgumentNullException(nameof(stateManager));

            _actions = new MenuActions();
        }

        public void Initialize()
        {
            _playState = _stateManager.GetState<PlayState>() ?? throw new NullReferenceException(nameof(PlayState));
        }

        public void Enter()
        {
        }

        public void Exit()
        {
            _actions.Reset();
        }

        public void Update()
        {
            _playState.Update();

            if (_actions.IsEnteringPlayState) { _stateManager.SetState<PlayState>(true, false); return; }
        }

        public void Input()
        {
            if (IsKeyPressed(Raylib.Raylib.KEY_R))
            {
                _actions.IsEnteringPlayState = true;
            }
        }

        public void Draw(float delta)
        {
            _playState.Draw(delta);
            DrawRectangleRec(new Raylib.Rectangle(0, 0, 1280, 720), new Raylib.Color(50, 50, 50, 90));

            DrawText("menu", 1280 / 2 - MeasureText("menu", 20) / 2, 720 / 8 - 20, 20, Raylib.Color.WHITE);
        }

        private class MenuActions
        {
            public bool IsEnteringPlayState { get; set; }

            public void Reset()
            {
                IsEnteringPlayState = false;
            }
        }
    }
}