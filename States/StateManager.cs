using System;
using System.Collections.Generic;

namespace Murk.States
{
    public class StateManager
    {
        private readonly Dictionary<Type, IState> _states;
        private IState _stateCurrent;
        private IState _stateScheduled;
        private bool _callExit;
        private bool _callEnter;

        public StateManager()
        {
            _states = new Dictionary<Type, IState>();
        }

        public void Initialize(params IState[] states)
        {
            // register all states so we can provide them when they're asked for
            foreach (var state in states)
            {
                _states[state.GetType()] = state;
            }

            // initialize all states
            foreach (var state in states)
            {
                state.Initialize();
            }

            // set a default state so we don't get any null exceptions
            _stateCurrent = NullState.Instance;
        }

        public void Update()
        {
            _stateCurrent.Update();

            if (_stateScheduled == null) { return; }

            if (_callExit) { _stateCurrent.Exit(); }
            if (_callEnter) { _stateScheduled.Enter(); }

            _stateCurrent = _stateScheduled;
            _stateScheduled = null;
        }

        public void Input()
        {
            _stateCurrent.Input();
        }
        
        public void Draw(float delta)
        {
            _stateCurrent.Draw(delta);
        }

        public void SetState<TState>(bool callExit = true, bool callEnter = true) where TState : IState
        {
            _stateScheduled = GetState<TState>();
            _callExit = callExit;
            _callEnter = callEnter;
        }

        public T GetState<T>() where T : IState
        {
            return (T)_states[typeof(T)];
        }

        private class NullState : IState
        {
            public static NullState Instance { get; } = new NullState();
            private NullState(){}
            public void Initialize(){}
            public void Enter(){}
            public void Exit(){}
            public void Update(){}
            public void Input(){}
            public void Draw(float delta){}
        }
    }
}