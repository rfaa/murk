using System;
using Murk.Networking;
using static Raylib.Raylib;

namespace Murk.States
{
    public class MainState : IState
    {
        private readonly StateManager _stateManager;
        private readonly NetworkClientManager _networkClientManager;
        private readonly NetworkServerManager _networkServerManager;
        private readonly MainActions _actions;

        public MainState(StateManager stateManager, NetworkClientManager networkClientManager, 
            NetworkServerManager networkServerManager)
        {
            _stateManager = stateManager ?? throw new ArgumentNullException(nameof(stateManager));
            _networkClientManager = networkClientManager ?? throw new ArgumentNullException(nameof(networkClientManager));
            _networkServerManager = networkServerManager ?? throw new ArgumentNullException(nameof(networkServerManager));

            _actions = new MainActions();
        }

        public void Initialize()
        {
        }

        public void Enter()
        {
            _networkClientManager.OnConnected += OnConnected;
            _networkClientManager.OnDisconnected += OnDisconnected;
        }

        public void Exit()
        {
            _networkClientManager.OnConnected -= OnConnected;
            _networkClientManager.OnDisconnected -= OnDisconnected;

            _actions.Reset();
        }

        public void Update()
        {
            if (_actions.IsEnteringLobbyState) { _stateManager.SetState<LobbyState>(); return; }

            if (_actions.IsConnecting) { return; }

            if (_actions.IsCreatingGame)
            {
                if (_networkServerManager.Start()) { _actions.IsJoiningGame = true; }
                _actions.IsCreatingGame = false;
            }

            if (_actions.IsJoiningGame)
            {
                _networkClientManager.Connect("127.0.0.1");
                _actions.IsConnecting = true;
                _actions.IsJoiningGame = false;
            }
        }

        public void Input()
        {
            if (_actions.IsConnecting) { return; }

            if (IsKeyPressed((int)Raylib.KeyboardKey.KEY_C)) { _actions.IsCreatingGame = true; }

            if (IsKeyPressed((int)Raylib.KeyboardKey.KEY_J)) { _actions.IsJoiningGame = true; }
        }

        public void Draw(float delta)
        {
            DrawText("main", 1280 / 2 - MeasureText("main", 20) / 2, 720 / 8, 20, Raylib.Color.WHITE);

            if (_actions.IsConnecting)
            {
                DrawText("connecting", 1280 / 2 - MeasureText("connecting", 10) / 2, 720 / 6, 10, Raylib.Color.WHITE);
            }

            DrawText("[c]reate", 1280 / 2 - MeasureText("[c]reate", 10) / 2, 720 / 4, 10, Raylib.Color.WHITE);
            DrawText("[j]oin", 1280 / 2 - MeasureText("[j]oin", 10) / 2, 720 / 4 + 12, 10, Raylib.Color.WHITE);
        }

        private void OnConnected()
        {
            _actions.IsConnecting = false;
            _actions.IsEnteringLobbyState = true;
        }

        private void OnDisconnected()
        {
            _actions.IsConnecting = false;
        }

        private class MainActions
        {
            public bool IsEnteringLobbyState { get; set; }
            public bool IsCreatingGame { get; set; }
            public bool IsJoiningGame { get; set; }
            public bool IsConnecting { get; set; }

            public void Reset()
            {
                IsEnteringLobbyState = false;
                IsCreatingGame = false;
                IsJoiningGame = false;
                IsConnecting = false;
            }
        }
    }
}