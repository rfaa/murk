using LiteNetLib.Utils;
using Murk.Utilities;

namespace Murk.Commands
{
    public interface ICommand : INetSerializable, IPoolable
    {
        CommandType Type { get; }
        void Execute();
    }
}