using System;
using System.Collections.Generic;
using Murk.Entities;
using Murk.Networking.Packets.Play;
using Murk.Utilities;

namespace Murk.Commands
{
    public class CommandFactory
    {
        private readonly Dictionary<CommandType, Pool<ICommand>> _pools;
        private readonly Dictionary<Type, CommandType> _types;
        private readonly EntityFactory _entityFactory;

        public CommandFactory(EntityFactory entityFactory)
        {
            _entityFactory = entityFactory ?? throw new ArgumentNullException(nameof(entityFactory));

            _pools = new Dictionary<CommandType, Pool<ICommand>>();
            _types = new Dictionary<Type, CommandType>();
        }

        public void Initialize()
        {
            InitializePool(CommandType.Spawn, () => new SpawnCommand(_entityFactory));
        }

        public T Get<T>() where T : class, ICommand => Get<T>(x => {});

        public T Get<T>(Action<T> configure) where T : class, ICommand
        {
            var command = (T)_pools[GetCommandType<T>()].Get();
            configure(command);
            return command;
        }

        public ICommand Get(CommandType type)
        {
            return _pools[type].Get();
        }

        public void Recycle(ICommand command)
        {
            _pools[command.Type].Recycle(command);
        }

        public CommandType GetCommandType<T>() where T : class, ICommand
        {
            return _types[typeof(T)];
        }

        private void InitializePool<T>(CommandType type, Func<T> ctor) where T : class, ICommand
        {
            _pools[(_types[typeof(T)] = type)] = new Pool<ICommand>(ctor, 256);
        }
    }
}