using System;
using LiteNetLib.Utils;
using Murk.Components.Attributes;
using Murk.Entities;

namespace Murk.Commands
{
    public class SpawnCommand : ICommand
    {
        public CommandType Type => CommandType.Spawn;
        public int X { get; set; }
        public int Y { get; set; }
        public EntityType EntityType { get; set; }

        private readonly EntityFactory _entityFactory;

        public SpawnCommand(EntityFactory entityFactory)
        {
            _entityFactory = entityFactory ?? throw new ArgumentNullException(nameof(entityFactory));
        }

        public void Execute()
        {
            _entityFactory.Get(EntityType, entity =>
            {
                if (entity.TryGetComponent<BodyComponent>(out var body))
                {
                    body.X = body.PreviousX = X;
                    body.Y = body.PreviousY = Y;
                }
            });
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put((byte)Type);
            writer.Put(X);
            writer.Put(Y);
            writer.Put((byte)EntityType);
        }

        public void Deserialize(NetDataReader reader)
        {
            X = reader.GetInt();
            Y = reader.GetInt();
            EntityType = (EntityType)reader.GetByte();
        }

        public void Reset()
        {
            X = 0;
            Y = 0;
            EntityType = EntityType.None;
        }
    }
}