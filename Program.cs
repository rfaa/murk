﻿using System;

namespace Murk
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var engine = new Engine())
            {
                engine.Initialize();
                engine.Run();
            }
        }
    }
}
