using System;
using System.Collections.Generic;
using System.Linq;
using Murk.Components;
using Murk.Utilities;

namespace Murk.Entities
{
    public class Entity : IPoolable
    {
        public EntityType Type { get; set; }
        public int Id { get; private set; }

        private readonly EntityFactory _entityFactory;
        private readonly ComponentManager _componentManager;
        private readonly ComponentFactory _componentFactory;
        private readonly Dictionary<Type, (IComponent component, Action recycler)> _components;

        public Entity(int id, EntityFactory entityFactory, ComponentManager componentManager, ComponentFactory componentFactory)
        {
            Id = id;
            _entityFactory = entityFactory ?? throw new ArgumentNullException(nameof(entityFactory));
            _componentManager = componentManager ?? throw new ArgumentNullException(nameof(componentManager));
            _componentFactory = componentFactory ?? throw new ArgumentNullException(nameof(componentFactory));

            _components = new Dictionary<Type, (IComponent, Action)>();
        }

        public void Die()
        {
            _entityFactory.Recycle(this);
        }

        public void Reset()
        {
            var nodes = _components.Values.ToArray();
            foreach (var node in nodes)
            {
                _components.Remove(node.component.GetType());
                node.recycler();
            }

            Type = EntityType.None;
        }

        public bool TryAddComponent<T>() where T : class, IComponent, new()
        {
            return TryAddComponent<T>(x => {});
        }

        public bool TryAddComponent<T>(Action<T> configure) where T : class, IComponent, new()
        {
            if (_components.ContainsKey(typeof(T))) { return false; }

            var component = _componentFactory.Get<T>();
            configure(component);
            _components.Add(typeof(T), 
                (component, 
                () => RecycleComponent(component)));
            _componentManager.ComponentAdded(component, this);

            return true;
        }

        public bool TryRemoveComponent<T>() where T : class, IComponent, new()
        {
            if (!_components.TryGetValue(typeof(T), out var node)) { return false; }

            node.recycler();
            _components.Remove(typeof(T));

            return true;
        }

        public bool TryGetComponent<T>(out T component) where T : class, IComponent, new()
        {
            if (!_components.TryGetValue(typeof(T), out var node)) 
            { 
                component = null;
                return false; 
            }

            component = (T)node.component;
            return true;
        }

        private void RecycleComponent<T>(T component) where T : class, IComponent, new()
        {
            _componentManager.ComponentRemoved(component, this);
            _componentFactory.Recycle(component);
        }
    }
}