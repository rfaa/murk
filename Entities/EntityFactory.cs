using System;
using System.Collections.Generic;
using Murk.Components;
using Murk.Components.Attributes;
using Murk.Utilities;

namespace Murk.Entities
{
    public class EntityFactory
    {
        public event Action<Entity> OnSpawn;
        public event Action<Entity> OnDeath;

        private readonly ComponentManager _componentManager;
        private readonly ComponentFactory _componentFactory;
        private readonly Pool<Entity> _pool;
        private readonly Dictionary<EntityType, Func<Entity, Entity>> _constructors;

        private int _idCounter;

        public EntityFactory(ComponentManager componentManager, ComponentFactory componentFactory)
        {
            _componentManager = componentManager ?? throw new ArgumentNullException(nameof(componentManager));
            _componentFactory = componentFactory ?? throw new ArgumentNullException(nameof(componentFactory));
            
            _pool = new Pool<Entity>(() => new Entity(_idCounter++, this, _componentManager, _componentFactory), 512);
            _constructors = new Dictionary<EntityType, Func<Entity, Entity>>();
        }

        public void Initialize()
        {
            _constructors.Add(EntityType.Ship, ConstructShip);
        }

        public void Reset()
        {
            _idCounter = 0;
            _pool.Reset();
        }

        public Entity Get(EntityType type) => Get(type, x => {});

        public Entity Get(EntityType type, Action<Entity> configure)
        {
            var entity = _pool.Get();
            _constructors[type](entity);
            configure(entity);
            OnSpawn?.Invoke(entity);
            return entity;
        }

        public void Recycle(Entity entity)
        {
            OnDeath?.Invoke(entity);
            _pool.Recycle(entity);
        }

        private Entity ConstructShip(Entity entity)
        {
            entity.Type = EntityType.Ship;
            entity.TryAddComponent<BodyComponent>(x => 
            {
                x.X = 500;
                x.Y = 500;
                x.Radius = 1000;
                x.Speed = 50;
            });

            return entity;
        }
    }
}