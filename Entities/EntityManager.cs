using System;
using System.Collections.Generic;

namespace Murk.Entities
{
    public class EntityManager
    {
        public int EntityCount { get => _entities.Count; }

        private readonly EntityFactory _entityFactory;
        private readonly Dictionary<int, Entity> _entities;

        public EntityManager(EntityFactory entityFactory)
        {
            _entityFactory = entityFactory ?? throw new ArgumentNullException(nameof(entityFactory));
            _entities = new Dictionary<int, Entity>();
        }

        public void Initialize()
        {
            _entityFactory.OnSpawn += OnSpawn;
            _entityFactory.OnDeath += OnDeath;
        }

        public bool TryGetEntity(int id, out Entity entity)
        {
            return _entities.TryGetValue(id, out entity);
        }

        private void OnSpawn(Entity entity)
        {
            _entities.Add(entity.Id, entity);
        }

        private void OnDeath(Entity entity)
        {
            _entities.Remove(entity.Id);
        }
    }
}