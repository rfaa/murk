using System;
using System.Collections.Generic;
using Murk.Components;
using Murk.Components.Attributes;
using Murk.Entities;
using Raylib;
using static Raylib.Raylib;

namespace Murk.Systems.Draw
{
    public class BodyDrawSystem : IDrawSystem
    {
        private readonly ComponentManager _componentManager;
        private readonly Dictionary<int, int> _indexes;
        private (BodyComponent body, Entity entity)[] _nodes;
        private int _index;

        public BodyDrawSystem(ComponentManager componentManager)
        {
            _componentManager = componentManager ?? throw new ArgumentNullException(nameof(componentManager));
            _nodes = new (BodyComponent, Entity)[32];
            _indexes = new Dictionary<int, int>();
        }

        public void Initialize()
        {
            _componentManager.Subscribe<BodyComponent>(OnBodyComponentAdded, OnBodyComponentRemoved);
        }

        public void Draw(float delta)
        {
            for (var i = _index - 1; i >= 0; i--)
            {
                var node = _nodes[i];
                var body = node.body;

                DrawCircleLines(
                    centerX: Utilities.Math.Lerp(body.PreviousX, body.X, delta) / 100,
                    centerY: Utilities.Math.Lerp(body.PreviousY, body.Y, delta) / 100,
                    radius: Utilities.Math.Lerp(body.PreviousRadius, body.Radius, delta) / 100,
                    color: Color.WHITE);
            }
        }

        private void OnBodyComponentAdded(BodyComponent body, Entity entity)
        {
            if (_index == _nodes.Length) { Array.Resize(ref _nodes, _nodes.Length * 2); }

            _indexes[entity.Id] = _index;
            _nodes[_index++] = (body, entity);
        }

        private void OnBodyComponentRemoved(BodyComponent body, Entity entity)
        {
            var index = _indexes[entity.Id];
            var node = _nodes[index];

            _indexes.Remove(entity.Id);

            // don't put the last node in the removed node's spot if they are the same
            if (index != --_index)
            {
                node = _nodes[index] = _nodes[_index];
                _indexes[node.entity.Id] = index;
            }
        }
    }
}