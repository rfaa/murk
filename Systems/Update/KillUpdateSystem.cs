using System;
using System.Collections.Generic;
using System.Linq;
using Murk.Components;
using Murk.Components.Attributes;
using Murk.Components.Utilities;
using Murk.Entities;

namespace Murk.Systems.Update
{
    public class KillUpdateSystem : IUpdateSystem
    {
        private readonly EntityFactory _entityFactory;
        private readonly ComponentFactory _componentFactory;
        private readonly Dictionary<int, int> _indexes;
        private (CounterComponent counter, Entity entity)[] _nodes;
        private int _index;

        public KillUpdateSystem(EntityFactory entityFactory, ComponentFactory componentFactory)
        {
            _entityFactory = entityFactory ?? throw new ArgumentNullException(nameof(entityFactory));
            _componentFactory = componentFactory ?? throw new ArgumentNullException(nameof(componentFactory));

            _nodes = new (CounterComponent, Entity)[32];
            _indexes = new Dictionary<int, int>();
        }

        public void Initialize()
        {
            _entityFactory.OnSpawn += OnSpawn;
            _entityFactory.OnDeath += OnDeath;
        }

        public void Update()
        {
            for (var i = _index - 1; i >= 0; i--)
            {
                var node = _nodes[i];
                var counter = node.counter;
                var entity = node.entity;

                if (++counter.Counter > 60)
                {
                    entity.Die();
                }
            }
        }

        private void OnSpawn(Entity entity)
        {
            if (_index == _nodes.Length) { Array.Resize(ref _nodes, _nodes.Length * 2); }

            _indexes[entity.Id] = _index;
            _nodes[_index++] = (_componentFactory.Get<CounterComponent>(), entity);
        }

        private void OnDeath(Entity entity)
        {
            var index = _indexes[entity.Id];
            var node = _nodes[index];

            _componentFactory.Recycle(node.counter);
            _indexes.Remove(entity.Id);

            // don't put the last node in the removed node's spot if they are the same
            if (index != --_index) 
            {
                node = _nodes[index] = _nodes[_index];
                _indexes[node.entity.Id] = index;
            }
        }
    }
}