using System;
using System.Collections.Generic;
using Murk.Components;
using Murk.Components.Attributes;
using Murk.Components.Utilities;
using Murk.Entities;

namespace Murk.Systems.Update
{
    public class MovementUpdateSystem : IUpdateSystem
    {
        private readonly ComponentManager _componentManager;
        private readonly ComponentFactory _componentFactory;
        private readonly Dictionary<int, int> _indexes;
        private (BodyComponent body, ToggleComponent toggle, Entity entity)[] _nodes;
        private int _index;

        public MovementUpdateSystem(ComponentManager componentManager, ComponentFactory componentFactory)
        {
            _componentManager = componentManager ?? throw new ArgumentNullException(nameof(componentManager));
            _componentFactory = componentFactory ?? throw new ArgumentNullException(nameof(componentFactory));

            _nodes = new (BodyComponent, ToggleComponent, Entity)[32];
            _indexes = new Dictionary<int, int>();
        }

        public void Initialize()
        {
            _componentManager.Subscribe<BodyComponent>(OnBodyComponentAdded, OnBodyComponentRemoved);
        }

        public void Update()
        {
            for (var i = _index - 1; i >= 0; i--)
            {
                var node = _nodes[i];
                var body = node.body;
                var toggle = node.toggle;
                
                body.X += toggle.Toggle ? -body.Speed : body.Speed;
                body.Radius += 50;

                if (body.X + body.Radius >= 128000)
                {
                    body.X = 128000 - body.Radius;
                    toggle.Toggle = !toggle.Toggle;
                }
                else if (body.X - body.Radius <= 0)
                {
                    body.X = 0 + body.Radius;
                    toggle.Toggle = !toggle.Toggle;
                }
            }
        }

        private void OnBodyComponentAdded(BodyComponent body, Entity entity)
        {
            if (_index == _nodes.Length) { Array.Resize(ref _nodes, _nodes.Length * 2); }

            _indexes[entity.Id] = _index;
            _nodes[_index++] = (body, _componentFactory.Get<ToggleComponent>(), entity);
        }

        private void OnBodyComponentRemoved(BodyComponent body, Entity entity)
        {
            var index = _indexes[entity.Id];
            var node = _nodes[index];

            _componentFactory.Recycle(node.toggle);
            _indexes.Remove(entity.Id);

            // don't put the last node in the removed node's spot if they are the same
            if (index != --_index)
            {
                node = _nodes[index] = _nodes[_index];
                _indexes[node.entity.Id] = index;
            }
        }
    }
}