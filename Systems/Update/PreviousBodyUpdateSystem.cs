using System;
using System.Collections.Generic;
using Murk.Components;
using Murk.Components.Attributes;
using Murk.Entities;

namespace Murk.Systems.Update
{
    public class PreviousBodyUpdateSystem : IUpdateSystem
    {
        private readonly ComponentManager _componentManager;
        private readonly Dictionary<int, int> _indexes;
        private (BodyComponent body, Entity entity)[] _nodes;
        private int _index;

        public PreviousBodyUpdateSystem(ComponentManager componentManager)
        {
            _componentManager = componentManager ?? throw new ArgumentNullException(nameof(componentManager));
            _nodes = new (BodyComponent, Entity)[32];
            _indexes = new Dictionary<int, int>();
        }

        public void Initialize()
        {
            _componentManager.Subscribe<BodyComponent>(OnBodyComponentAdded, OnBodyComponentRemoved);
        }

        public void Update()
        {
            for (var i = _index - 1; i >= 0; i--)
            {
                var node = _nodes[i];
                var body = node.body;

                body.PreviousX = body.X;
                body.PreviousY = body.Y;
                body.PreviousRadius = body.Radius;
            }
        }

        private void OnBodyComponentAdded(BodyComponent body, Entity entity)
        {
            if (_index == _nodes.Length) { Array.Resize(ref _nodes, _nodes.Length * 2); }

            _indexes[entity.Id] = _index;
            _nodes[_index++] = (body, entity);
        }

        private void OnBodyComponentRemoved(BodyComponent body, Entity entity)
        {
            var index = _indexes[entity.Id];
            var node = _nodes[index];

            _indexes.Remove(entity.Id);

            // don't put the last node in the removed node's spot if they are the same
            if (index != --_index)
            {
                node = _nodes[index] = _nodes[_index];
                _indexes[node.entity.Id] = index;
            }
        }
    }
}