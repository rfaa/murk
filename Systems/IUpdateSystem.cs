namespace Murk.Systems
{
    public interface IUpdateSystem
    {
        void Initialize();
        void Update();
    }
}