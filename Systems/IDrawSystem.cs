namespace Murk.Systems
{
    public interface IDrawSystem
    {
        void Initialize();
        void Draw(float delta);
    }
}