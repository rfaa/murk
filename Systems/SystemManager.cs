using System.Collections.Generic;

namespace Murk.Systems
{
    public class SystemManager
    {
        private readonly List<IUpdateSystem> _updateSystems;
        private readonly List<IDrawSystem> _drawSystems;

        public SystemManager()
        {
            _updateSystems = new List<IUpdateSystem>();
            _drawSystems = new List<IDrawSystem>();
        }

        public void Initialize(IUpdateSystem[] updateSystems, IDrawSystem[] drawSystems)
        {
            foreach (var updateSystem in updateSystems)
            {
                updateSystem.Initialize();
                _updateSystems.Add(updateSystem);
            }

            foreach (var drawSystem in drawSystems)
            {
                drawSystem.Initialize();
                _drawSystems.Add(drawSystem);
            }
        }

        public void Update()
        {
            foreach (var updateSystem in _updateSystems)
            {
                updateSystem.Update();
            }
        }

        public void Draw(float delta)
        {
            foreach (var drawSystem in _drawSystems)
            {
                drawSystem.Draw(delta);
            }
        }
    }
}