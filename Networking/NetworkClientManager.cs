using System;
using System.Collections.Generic;
using LiteNetLib;
using LiteNetLib.Utils;
using Murk.Networking.Packets;

namespace Murk.Networking
{
    public class NetworkClientManager
    {
        public event Action OnConnected;
        public event Action OnDisconnected;

        private readonly PacketFactory _packetFactory;
        private readonly NetManager _netManager;
        private readonly EventBasedNetListener _netListener;
        private readonly NetDataWriter _netDataWriter;
        private readonly Dictionary<PacketType, Dictionary<int, Action<IPacket>>> _subscriptions;

        public NetworkClientManager(PacketFactory packetFactory)
        {
            _packetFactory = packetFactory ?? throw new ArgumentNullException(nameof(packetFactory));

            _netListener = new EventBasedNetListener();
            _netManager = new NetManager(_netListener);
            _netDataWriter = new NetDataWriter();
            _subscriptions = new Dictionary<PacketType, Dictionary<int, Action<IPacket>>>();
        }

        public void Initialize()
        {
            _netManager.AutoRecycle = true;
            _netListener.NetworkReceiveEvent += OnPacketReceived;
            _netListener.PeerConnectedEvent += (NetPeer peer) => OnConnected?.Invoke();
            _netListener.PeerDisconnectedEvent += (NetPeer peer, DisconnectInfo info) => OnDisconnected?.Invoke();
        }

        public void Update()
        {
            _netManager.PollEvents();
        }

        public void Connect(string host, int port = 4392)
        {
            if (!_netManager.IsRunning && !_netManager.Start()) { OnDisconnected?.Invoke(); return; }
            _netManager.Connect(host, port, "murk");
        }

        public void Disconnect()
        {
            _netManager.DisconnectAll();
        }

        public void Send<T>() where T : class, IPacket => Send<T>(x => {});

        public void Send<T>(Action<T> configure) where T : class, IPacket
        {
            var packet = _packetFactory.Get<T>();
            configure(packet);

            _netDataWriter.Put((byte)packet.Type);
            packet.Serialize(_netDataWriter);
            _netManager.SendToAll(_netDataWriter, DeliveryMethod.ReliableOrdered);
            _netManager.Flush();

            _netDataWriter.Reset();
            _packetFactory.Recycle(packet);
        }

        public void Subscribe<T>(Action<T> onReceive) where T : class, IPacket
        {
            var type = _packetFactory.GetPacketType<T>();
            if (!_subscriptions.TryGetValue(type, out var subscriptions))
            {
                subscriptions = new Dictionary<int, Action<IPacket>>();
                _subscriptions.Add(type, subscriptions);
            }

            subscriptions.Add(onReceive.GetHashCode(), x => onReceive((T)x));
        }

        public void Unsubscribe<T>(Action<T> onReceive) where T : class, IPacket
        {
            var type = _packetFactory.GetPacketType<T>();
            if (_subscriptions.TryGetValue(type, out var subscriptions))
            {
                subscriptions.Remove(onReceive.GetHashCode());
            }
        }

        private void OnPacketReceived(NetPeer peer, NetPacketReader reader, DeliveryMethod method)
        {
            if (method != DeliveryMethod.ReliableOrdered) { return; }
            if (!reader.TryGetByte(out var type)) { return; }
            if (!_subscriptions.TryGetValue((PacketType)type, out var subscriptions)) { return; }

            var packet = _packetFactory.Get((PacketType)type);

            packet.Deserialize(reader);

            foreach (var subscription in subscriptions.Values)
            {
                subscription(packet);
            }

            _packetFactory.Recycle(packet);
        }
    }
}