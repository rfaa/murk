using System;
using LiteNetLib;
using Murk.Commands;
using Murk.Networking.Packets.Lobby;
using Murk.Networking.Packets.Play;

namespace Murk.Networking
{
    public class ServerManager
    {
        private readonly CommandFactory _commandFactory;
        private readonly NetworkServerManager _networkServerManager;
        private readonly int _tickIncrementor;

        public ServerManager(NetworkServerManager networkServerManager, CommandFactory commandFactory)
        {
            _networkServerManager = networkServerManager ?? throw new ArgumentNullException(nameof(networkServerManager));
            _commandFactory = commandFactory ?? throw new ArgumentNullException(nameof(commandFactory));

            _tickIncrementor = 2;
        }

        public void Initialize()
        {
            _networkServerManager.OnConnectionRequest += OnConnectionRequest;
            _networkServerManager.Subscribe<LobbyStartPacket>(OnLobbyStart);
            _networkServerManager.Subscribe<LobbyStopPacket>(OnLobbyStop);
            _networkServerManager.Subscribe<PlayUpdatePacket>(OnPlayUpdate);
            _networkServerManager.Subscribe<PlayStartPacket>(OnPlayStart);
        }

        private void OnConnectionRequest(ConnectionRequest request)
        {
            request.AcceptIfKey("murk");
        }

        private void OnLobbyStart(LobbyStartPacket packet, NetPeer peer)
        {
            _networkServerManager.SendToAll<LobbyStartPacket>(x => x.ClientId = peer.Id);
        }

        private void OnLobbyStop(LobbyStopPacket packet, NetPeer peer)
        {
            _networkServerManager.SendToAll<LobbyStopPacket>(x => x.ClientId = peer.Id);
        }

        private void OnPlayUpdate(PlayUpdatePacket packet, NetPeer peer)
        {
            _networkServerManager.SendToAll<PlayUpdatePacket>(x => 
            {
                x.ClientId = peer.Id;
                x.Tick = packet.Tick + _tickIncrementor;
                x.Commands = packet.Commands;
            });
        }

        private void OnPlayStart(PlayStartPacket packet, NetPeer peer)
        {
            // TODO: foreach client
            for (var i = 0; i < _tickIncrementor; i++)
            {
                _networkServerManager.SendToPeer<PlayUpdatePacket>(peer, x => 
                {
                    x.ClientId = peer.Id;
                    x.Tick = i;
                    x.Commands = new ICommand[0];
                }, false);
            }

            peer.Flush();
        }
    }
}