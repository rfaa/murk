namespace Murk.Networking.Packets
{
    public enum PacketType : byte
    {
        LobbyProgress,
        LobbyStart,
        LobbyStop,

        PlayStart,
        PlayUpdate,
        PlayPause,
        PlayResume
    }
}