using LiteNetLib.Utils;
using Murk.Utilities;

namespace Murk.Networking.Packets
{
    public interface IPacket : INetSerializable, IPoolable
    {
        PacketType Type { get; }
    }
}