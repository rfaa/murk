using System;
using System.Collections.Generic;
using Murk.Commands;
using Murk.Networking.Packets.Lobby;
using Murk.Networking.Packets.Play;
using Murk.Utilities;

namespace Murk.Networking.Packets
{
    public class PacketFactory
    {
        private readonly CommandFactory _commandFactory;
        private readonly Dictionary<PacketType, Pool<IPacket>> _pools;
        private readonly Dictionary<Type, PacketType> _types;

        public PacketFactory(CommandFactory commandFactory)
        {
            _commandFactory = commandFactory ?? throw new ArgumentNullException(nameof(commandFactory));

            _pools = new Dictionary<PacketType, Pool<IPacket>>();
            _types = new Dictionary<Type, PacketType>();
        }

        public void Initialize()
        {
            InitializePool(PacketType.LobbyProgress, () => new LobbyProgressPacket());
            InitializePool(PacketType.LobbyStart, () => new LobbyStartPacket());
            InitializePool(PacketType.LobbyStop, () => new LobbyStopPacket());
            
            InitializePool(PacketType.PlayStart, () => new PlayStartPacket());
            InitializePool(PacketType.PlayPause, () => new PlayPausePacket());
            InitializePool(PacketType.PlayResume, () => new PlayResumePacket());
            InitializePool(PacketType.PlayUpdate, () => new PlayUpdatePacket(_commandFactory));
        }

        public T Get<T>() where T : class, IPacket
        {
            return (T)_pools[GetPacketType<T>()].Get();
        }

        public IPacket Get(PacketType type)
        {
            return _pools[type].Get();
        }

        public void Recycle(IPacket packet)
        {
            _pools[packet.Type].Recycle(packet);
        }

        public PacketType GetPacketType<T>() where T : class, IPacket
        {
            return _types[typeof(T)];
        }

        private void InitializePool<T>(PacketType type, Func<T> ctor) where T : class, IPacket
        {
            _pools[(_types[typeof(T)] = type)] = new Pool<IPacket>(ctor, 32);
        }
    }
}