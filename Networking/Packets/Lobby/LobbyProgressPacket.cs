using System;
using LiteNetLib;
using LiteNetLib.Utils;

namespace Murk.Networking.Packets.Lobby
{
    public class LobbyProgressPacket : IPacket
    {
        public PacketType Type => PacketType.LobbyProgress;
        public int ClientId { get; set; }
        public int Progress { get; set; }

        public void Deserialize(NetDataReader reader)
        {
            var payload = (Payload)reader.GetByte();

            if (payload.HasFlag(Payload.ClientId)) { ClientId = reader.GetInt(); }

            Progress = reader.GetInt();
        }

        public void Serialize(NetDataWriter writer)
        {
            var payload = ClientId != 0 ? Payload.ClientId : Payload.None;
            writer.Put((byte)payload);

            if (payload.HasFlag(Payload.ClientId)) { writer.Put(ClientId); }

            writer.Put(Progress);
        }

        public void Reset()
        {
            ClientId = 0;
            Progress = 0;
        }

        [Flags]
        private enum Payload
        {
            None = 0,
            ClientId = 1
        }
    }
}