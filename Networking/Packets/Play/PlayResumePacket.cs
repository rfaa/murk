using System;
using LiteNetLib.Utils;

namespace Murk.Networking.Packets.Play
{
    public class PlayResumePacket : IPacket
    {
        public PacketType Type => PacketType.PlayResume;
        public int ClientId { get; set; }

        public void Deserialize(NetDataReader reader)
        {
            var payload = (Payload)reader.GetByte();

            if (payload.HasFlag(Payload.ClientId)) { ClientId = reader.GetInt(); }
        }

        public void Serialize(NetDataWriter writer)
        {
            var payload = ClientId != 0 ? Payload.ClientId : Payload.None;
            writer.Put((byte)payload);

            if (payload.HasFlag(Payload.ClientId)) { writer.Put(ClientId); }
        }

        public void Reset()
        {
            ClientId = 0;
        }

        [Flags]
        private enum Payload : byte
        {
            None = 0,
            ClientId = 1
        }
    }
}