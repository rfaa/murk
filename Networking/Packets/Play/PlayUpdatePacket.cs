using System;
using System.Buffers;
using System.Collections.Generic;
using LiteNetLib.Utils;
using Murk.Commands;

namespace Murk.Networking.Packets.Play
{
    public class PlayUpdatePacket : IPacket
    {
        public PacketType Type => PacketType.PlayUpdate;
        public int ClientId { get; set; }
        public int Tick { get; set; }
        public ICommand[] Commands { get; set; } = new ICommand[0];

        private readonly CommandFactory _commandFactory;

        public PlayUpdatePacket(CommandFactory commandFactory)
        {
            _commandFactory = commandFactory ?? throw new ArgumentNullException(nameof(commandFactory));
        }

        public void Deserialize(NetDataReader reader)
        {
            Tick = reader.GetInt();

            var payload = (Payload)reader.GetByte();
            if (payload.HasFlag(Payload.ClientId)) { ClientId = reader.GetInt(); }

            var size = reader.GetByte();
            Commands = new ICommand[size];
            for (var i = 0; i < size; i++)
            {
                var type = (CommandType)reader.GetByte();
                var command = _commandFactory.Get(type);
                command.Deserialize(reader);
                Commands[i] = command;
            }
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put(Tick);

            var payload = ClientId != 0 ? Payload.ClientId : Payload.None;
            writer.Put((byte)payload);

            if (payload.HasFlag(Payload.ClientId)) { writer.Put(ClientId); }
            
            writer.Put((byte)Commands.Length);
            for (var i = 0; i < Commands.Length; i++)
            {
                Commands[i].Serialize(writer);
            }
        }

        public void Reset()
        {
            ClientId = 0;
            Tick = 0;
            Commands = new ICommand[0];
        }

        [Flags]
        private enum Payload : byte
        {
            None = 0,
            ClientId = 1
        }
    }
}