using LiteNetLib.Utils;

namespace Murk.Networking.Packets.Play
{
    public class PlayStartPacket : IPacket
    {
        public PacketType Type => PacketType.PlayStart;

        public void Deserialize(NetDataReader reader){}
        public void Reset(){}
        public void Serialize(NetDataWriter writer){}
    }
}