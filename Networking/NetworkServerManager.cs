using System;
using System.Collections.Generic;
using LiteNetLib;
using LiteNetLib.Utils;
using Murk.Networking.Packets;

namespace Murk.Networking
{
    public class NetworkServerManager
    {
        public event Action<ConnectionRequest> OnConnectionRequest;

        private readonly PacketFactory _packetFactory;
        private readonly NetManager _netManager;
        private readonly EventBasedNetListener _netListener;
        private readonly NetDataWriter _netDataWriter;
        private readonly Dictionary<PacketType, Dictionary<int, Action<IPacket, NetPeer>>> _subscriptions;

        public NetworkServerManager(PacketFactory packetFactory)
        {
            _packetFactory = packetFactory ?? throw new ArgumentNullException(nameof(packetFactory));

            _netListener = new EventBasedNetListener();
            _netManager = new NetManager(_netListener);
            _netDataWriter = new NetDataWriter();
            _subscriptions = new Dictionary<PacketType, Dictionary<int, Action<IPacket, NetPeer>>>();
        }

        public void Initialize()
        {
            _netManager.AutoRecycle = true;
            _netListener.NetworkReceiveEvent += OnPacketReceived;
            _netListener.ConnectionRequestEvent += (ConnectionRequest request) => OnConnectionRequest?.Invoke(request);
        }

        public void Update()
        {
            _netManager.PollEvents();
        }

        public bool Start(int port = 4392)
        {
            return _netManager.IsRunning ? true : _netManager.Start(port);
        }

        public void Stop()
        {
            _netManager.Stop();
        }

        public void SendToAll<T>(bool flush = true) where T : class, IPacket
            => SendToAll<T>(x => {}, flush);

        public void SendToAll<T>(Action<T> configure, bool flush = true) where T : class, IPacket
            => Send<T>(configure, null, true, flush);

        public void SendToPeer<T>(NetPeer peer, bool flush = true) where T : class, IPacket
            => SendToPeer<T>(peer, x => {}, flush);

        public void SendToPeer<T>(NetPeer peer, Action<T> configure, bool flush = true) where T : class, IPacket
            => Send<T>(configure, peer, false, flush);

        private void Send<T>(Action<T> configure, NetPeer peer, bool sendToAll, bool flush = true) where T : class, IPacket
        {
            var packet = _packetFactory.Get<T>();
            configure(packet);

            _netDataWriter.Put((byte)packet.Type);
            packet.Serialize(_netDataWriter);
            
            if (sendToAll)
            {
                _netManager.SendToAll(_netDataWriter, DeliveryMethod.ReliableOrdered);
                if (flush) { _netManager.Flush(); }
            }
            else
            {
                peer.Send(_netDataWriter, DeliveryMethod.ReliableOrdered);
                if (flush) { peer.Flush(); }
            }

            _netDataWriter.Reset();
            _packetFactory.Recycle(packet);
        }

        public void Subscribe<T>(Action<T, NetPeer> onReceive) where T : class, IPacket
        {
            var type = _packetFactory.GetPacketType<T>();
            if (!_subscriptions.TryGetValue(type, out var subscriptions))
            {
                subscriptions = new Dictionary<int, Action<IPacket, NetPeer>>();
                _subscriptions.Add(type, subscriptions);
            }

            subscriptions.Add(onReceive.GetHashCode(), (packet, peer) => onReceive((T)packet, peer));
        }

        public void Unsubscribe<T>(Action<T, NetPeer> onReceive) where T : class, IPacket
        {
            var type = _packetFactory.GetPacketType<T>();
            if (_subscriptions.TryGetValue(type, out var subscriptions))
            {
                subscriptions.Remove(onReceive.GetHashCode());
            }
        }

        private void OnPacketReceived(NetPeer peer, NetPacketReader reader, DeliveryMethod method)
        {
            if (method != DeliveryMethod.ReliableOrdered) { return; }
            if (!reader.TryGetByte(out var type)) { return; }
            if (!_subscriptions.TryGetValue((PacketType)type, out var subscriptions)) { return; }
            
            var packet = _packetFactory.Get((PacketType)type);
            packet.Deserialize(reader);

            foreach (var subscription in subscriptions.Values)
            {
                subscription(packet, peer);
            }

            _packetFactory.Recycle(packet);
        }
    }
}